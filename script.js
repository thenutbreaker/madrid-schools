const fs = require('fs');
const fetch = require('node-fetch');

const PATH = "./";
const APIurl = "https://datos.madrid.es/egob/catalogo/202311-0-colegios-publicos.json" // OK

const CDD = [
    /.*manuel.*azaña.*/i,
    /.*miguel.*cervantes.*/i,
    /.*isabel.*catolica/i,
    /.*ortiz.*echague.*/i,
    /.*siglo.*xxi.*/i,
    /.*palacio.*valdés.*/i,
    /.*nicolas.*salmerón.*/i,
    /.*patriarca.*obispo.*/i,
    /.*conde.*romanones.*/i,
    /.*felipe.*ii.*/i,
    /.*ignacio.*zuluaga.*/i,
    /.*pio.*xii.*/i,
    /.*claudio.*moyano.*/i,
    /.*federico.*lorca.*/i,
    /.*jose.*bergamín.*/i,
    /.*república.*paraguay.*/i,
    /.*francisco.*goya.*/i,
    /.*joaquin.*dicenta.*/i,
    /.*república.*uruguay.*/i,
    /.*capitán.*cortés.*/i,
    /.*lope.*vega.*/i,
    /.*república.*colombia.*/i,
    /.*ciudad.*jaén.*/i,
    /.*gloria.*fuertes.*/i,
    /.*marcelo.*usera.*/i,
    /.*meseta.*orcasitas.*/i,
    /.*fuencisla.*/i,
    /.*república.*venezuela.*/i,
    /.*república.*brasil.*/i,
    /.*aragón.*/i,
    /.*asturias.*/i,
    /.*concha.*espina.*/i,
    /.*eduardo.*rojo.*/i,
    /.*madroño.*/i,
    /.*garcía.*morente.*/i,
    /.*giner.*ríos.*/i,
    /.*jose.*pereda.*/i,
    /.*manuel.*núñez.*arenas.*/i,
    /.*manuel.*nuñez.*arenas.*/i,
    /.*padre.*mariana.*/i,
    /.*francisco.*luis.*/i,
    /.*nuestra.*señora.*concepción.*/i,
    /.*méndez.*núñez.*/i,
    /.*méndez.*nuñez.*/i,
    /.*azorín.*/i,
    /.*navas.*tolosa.*/i,
    /.*república.*salvador.*/i,
    /.*san.*roque.*/i,
    /.*doctor.*severo.*ochoa.*/i,
    /.*alameda.*/i,
    /.*república.*panamá.*/i,
    /.*san.*pío.*x.*/i,
    /.*divino.*maestro.*/i,
    /.*clara.*campoamor.*/i,
    /.*ramón.*cajal.*/i
];


const filterGraph = json => json['@graph'];

const getLastWordFromURI = uri => {
    const array = uri.split('/');
    return array[array.length-1];
}

const parseLocation = school => ({
    type: "Point",
    coordinates: Object.values(school.location).reverse()
})
const parseSchoolDistrict = school => getLastWordFromURI(school.address.district['@id']);
const parseSchoolNeightbourhood = school => getLastWordFromURI(school.address.area['@id']);
const parseSchoolAddress = school => ({
    locality: school.address.locality,
    district: parseSchoolDistrict(school),
    neighbourhood: parseSchoolNeightbourhood(school),
    street: school.address['street-address'],
    postalcode: school.address['postal-code']
});

const parseSchoolEducation = school => {
    const e = school.organization['organization-desc'];
    let education = e.replace(/(bus|metro).*/img, '');
    education = education.replace(/biling&amp;uuml;e/gi, 'bilingüe');
    education = education.split(/(ENSEÑANZA(S)?:\s?)(ENSEÑANZA(S)?:\s?.*?)?/gi)[5]; 
    if (!education) education = e; // Estos casos habrá que revisarlos a mano
    return education.split("-").map(e => e.trim()).filter(e => e!=="");
}

const parseSchoolFeatures = school => 
    school.organization.services
        .split("-")
        .map(e => e.trim().replace(/\.$/, ''))
        .filter(e => e !== "");

const isHardPerformanceSchool = school => CDD.some(rx => rx.test(school.title));

const mapResponseToSchoolObject = res => res.map(e => ({
    school_id: parseInt(e.id),
    name: e.title,
    address: parseSchoolAddress(e),
    education: parseSchoolEducation(e),
    accessibility: e.organization.accessibility > 0,
    hard: isHardPerformanceSchool(e),
    features: parseSchoolFeatures(e),
    location: parseLocation(e)
}));

const main = (filename='schools.json') => {
    fetch(APIurl)
        .then(res => res.json())
        .then(json => {
            const schools =mapResponseToSchoolObject(filterGraph(json));
            console.log('Parsed', schools.length, 'files');
            fs.writeFile(PATH + "/out/" + filename, JSON.stringify(schools), err => {
                if (err) console.log(err);
                console.log('Saved schools.json');
            });
        }).catch(err => {
            console.error(err);
            onerror(err);
        });
};

main();
