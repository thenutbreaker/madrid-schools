# madrid-schools
## Prerequisites
- NPM
- NodeJS
- MongoDB

## Installation
First of all, project dependencies need to be installed with:
```bash
 $ npm install
```

And that's all! Go to [Usage](#usage)

## Usage
We just need to follow these simple steps to get a `json` containing all the schools that will be imported:
```bash
 $ cd <project_dir>
```
```bash
 $ node script.js
```
Then we need to connect to the Mongo database while being in the project directory and execute:
```javascript
load("import_data.js")
```

*et voilà!*

Happy searching.
